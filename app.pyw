from tkinter import *
# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras
import efficientnet.tfkeras
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image

# Some utilites
import numpy as np

#load model
SIZE=456
model = load_model('models/eff1.h5')
def loadEffecient() :
    global SIZE
    SIZE=456
    global model
    R1.select()
    model = load_model('models/eff1.h5')
def loadDense() :
    global SIZE
    SIZE=224
    global model
    R2.select()
    model = load_model('models/dens1.h5')
def loadRes () :
    global SIZE
    SIZE=224
    global model
    R3.select()
    model = load_model('models/res1.h5')

import os    
import cv2
import numpy as np
import pandas as pd
from tqdm import tqdm

def model_predict(img, model):
	
    image = cv2.resize(img, (SIZE, SIZE))
    score_predict = model.predict((image[np.newaxis])/255)
    # Process your result for human
    pred_proba = "{:.3f}".format(np.amax(score_predict))    # Max probability
    label_predict = np.argmax(score_predict)   # label predicted
    if label_predict == 0 :
            rst="Pas de Rétinopathie Diabétique"
    elif label_predict == 1 :
            rst="Rétinopathie Diabétique Minime"
    elif label_predict == 2 :
            rst="Rétinopathie Diabétique Modéré"
    elif label_predict == 3 :
            rst="Rétinopathie Diabétique Sévère"
    elif label_predict == 4 :
            rst="Rétinopathie Diabétique Proliférative"
    return (label_predict,rst,pred_proba,score_predict)


from tkinter import filedialog
from PIL import ImageTk, Image
import os
#GUI
root = Tk()
root.title("RetinoConv")
root.iconbitmap('img/iconret.ico')
root['bg']='#2F4F4F'
width=int(root.winfo_screenwidth()/2)
height=int(root.winfo_screenheight()/4)
root["height"]=height
root["width"]=width
cover= Image.open("img/Diabetic-Retina.jpg")
cover = cover.resize((width,height))
cover = ImageTk.PhotoImage(cover)
panel = Label(root,bg='#2F4F4F', image = cover)
panel.pack(side = "top", fill = "both", expand = "yes")
title_label= Label(root, text = 'Classificateur  De Rétinopathie Diabétique',fg='white',bg='#2F4F4F',font=('Helvetica',30,"bold")).pack(pady=20)
title_label1= Label(root, text = "Importer l'image pour la classifier :",bg='#2F4F4F',fg='white', font=('times',12)).pack(pady=20)


#upload and predict function 
def OpenImgAndPred() :
    file = filedialog.askopenfilename(initialdir = '/' ,parent=root,title="Choisir l'image", filetypes = (( "png files" , "* . png " ), ( "jpeg files" , "* .jpg" )) )
    print(file)
    retinaimg=Image.open(file)
    retinaimg = retinaimg.resize((200,200))
    retinaimg = ImageTk.PhotoImage(retinaimg)
    panelr.config(image=retinaimg)
    
    panelr.photo = retinaimg

    if file!= None :
        fileimg = cv2.imread(file)
        label,labelname,prob,scores=model_predict(fileimg ,model)
        print(label)
        print(labelname)
        print(prob)
        print(scores)
        prob=float(prob)*100
        prob='{0:.3f}'.format(prob)
        probs=str(prob) 
		#prob_label['fg']='red'
		#labelname['fg']='blue'
        prob_label['text']="Probabilité = "+probs+" %"
        labelname_label['text'] ="Sévérité : "+labelname

R1 = Radiobutton(root, text="EfficientNet",bg='#2F4F4F' ,value=0,command=loadEffecient )
R2 = Radiobutton(root, text="DenseNet",bg='#2F4F4F' ,value=1,command=loadDense)
R3 = Radiobutton(root, text="ResNet",bg='#2F4F4F' ,value=2,command=loadRes)
                 
panelr = Label(root,bg='#2F4F4F')       
prob_label= Label(root,fg='white',bg='#2F4F4F',font=('times',15))
labelname_label= Label(root,fg='white',bg='#2F4F4F',font=('times',15))
B = Button(root, text ="Upload", width = 20,bg='black',fg="white", font=(20) , command = OpenImgAndPred).pack(side='top',pady=5)



ch_label= Label(root, text = "Choisir le modele :",bg='#2F4F4F',fg='white').pack(anchor=W)                
R1.pack( anchor = W )
R1.select()

                
R2.pack( anchor = W )


R3.pack( anchor = W)

panelr.pack()
labelname_label.pack(pady=10)
prob_label.pack(pady=10)
Bylabel= Label(root, text = 'Par : Benhabiles Adlane & Boudjema Mohamed Abdelwahab',bg='#2F4F4F', font=('times',12)).pack(pady=25)






root.mainloop()