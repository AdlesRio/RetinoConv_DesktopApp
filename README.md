# RetinoConv_DesktopApp
RetinoConv Retinopathy Diabetic Classifier Desktop App

## Il est facile à exécuter sur votre ordinateur.

# 1. Tout d'abord, clonez le dépôt
$ git clone https://gitlab.com/AdlesRio/RetinoConv_DesktopApp.git

# 2. Installez les packages Python
$ pip install -r requirements.txt

# 3. executer le script
$ python app.pyw


L'application Desktop RetinoConv se lance  .